<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Listar</title>
    <!--libs css -->
    <link rel="stylesheet" href="../public/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../public/dataTable/css/dataTables.bootstrap4.min.css">

    <!--libs js -->
    <script src="../public/jquery/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="../public/dataTable/js/jquery.dataTables.min.js"></script>
    <script src="../public/dataTable/js/dataTables.bootstrap4.min.js"></script> 
    <script src="../public/vue/vue.js"></script>
    <script src="https://unpkg.com/vee-validate@2.0.0-beta.25"></script>
    <script src="../public/libs/dist/sweetalert.min.js"></script>
    <!--<script src="../public/vue/dist/vee-validate.js"></script>-->
    <style>
      @import url('https://unpkg.com/semantic-ui-css@2.2.9/semantic.css');

      span.error {
        color: #9F3A38;
      }
      html,body{
        background-image: url('../public/img/dashboard.png');
        background-size: 100% 125%;;
        background-repeat: no-repeat;
        height: 100%;
      }
      .card{
        width: 86% !important;
        margin-left: 8% !important;
        margin-top: 52px !important;
      }
      .btn-add-user {
        width: 170px;
        height: 39px; 
        padding: 0pc; 
        font-size: 17px;
      }
      .modal-content-cumston {
        margin-top: 74px; 
        position: absolute;
      }
      #text-label {
        margin-left: -12px;
      }
    </style>
  </head>
  <body>
  	<!--Navbar -->
  	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
  	    <span class="navbar-toggler-icon"></span>
  	  </button>
  	  <a class="navbar-brand" href="menu.php">Inicio</a>

  	  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
  	    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
  	      <li class="nav-item active">
  	        <a class="nav-link" href="#">Crear Tarea <span class="sr-only"></span></a>
  	      </li>
  	      <li class="nav-item active">
  	        <a class="nav-link" href="#">Listar Tareas<span class="sr-only"></span></a>
  	      </li>
  	      <li class="nav-item active">
  	        <a class="nav-link" href="list_user.php">Listar Usuarios<span class="sr-only"></span></a>
  	      </li>
  	    </ul>
  	    <form class="form-inline my-2 my-lg-0">
  	      <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Search">
  	      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
  	    </form>
  	  </div>
  	</nav>

    <div class="row" style="width: 100% !important;" id="app-table">
      <div class="col-md-12">
        <section class="panel"> 
          <div class="panel-body">
            <div class="card" style="margin-top: 16px !important;">
              <div class="card-header text-center font-weight-bold py-4" style="height: 17px !important;"><!-- text-uppercase -->
                <h3 style="font-size: 18px; margin-top: -12px; margin-left: -64%;">Usuarios</h3>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalUser" style="height:24px; width: 113px; text-align:center;  margin-top: -72px; margin-left: -89%; padding: 0px; font-size: 14px;" @click="addUser()">Crear Usuarios</button>
              </div>
              <div class="card-body">
                <div id="table">
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                    
                    <thead>
                      <tr>
                        <th class="text-center" hidden="">Id</th>
                        <th class="text-center">Nombres</th>
                        <th class="text-center">Apellidos</th>
                        <th class="text-center">usuario</th>
                        <th class="text-center">Area</th>
                        <th class="text-center">Perfil</th>
                        <th class="text-center">Activo</th>
                        <th class="text-center">Acciones</th>
                      </tr>
                    </thead>
                        
                    <tbody id="tbody-render">
                      <tr v-if="tableBody" v-for="(data, index) in tableData">
							    			<td class='pt-3-half text-center' id='id-table' contenteditable='false' hidden>{{ tableData[index].id }}</td> <!-- hidden -->
							    			<td class='pt-3-half text-center' contenteditable='false'>{{ tableData[index].nom_usuario }}</td>
							    			<td class='pt-3-half text-center' contenteditable='false'>{{ tableData[index].ape_usuario }}</td>
							    			<td class='pt-3-half text-center' contenteditable='false'>{{ tableData[index].usuario }}</td>
							    			<td class='pt-3-half text-center' contenteditable='false'>{{ tableData[index].nom_area }}</td>
                        <td class='pt-3-half text-center' contenteditable='false'>{{ tableData[index].nom_perfil }}</td>
							    			<td class='pt-3-half text-center' contenteditable='false'>{{ tableData[index].activo }}</td>
							    			<td class='pt-3-half text-center'>
							    			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="height:24px; width:67px; text-align:center; margin-top:-2px; padding: 0px; font-size: 14px;" @click="editUser(tableData[index])">Editar</button>
							    			</td>
                      </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                      <tr>
                        <td class="gutter">
                          <div class="line number1 index0 alt2" style="display: none;">1</div>
                        </td>
                        <td class="code">
                          <div class="container" style="display: none;">
                            <div class="line number1 index0 alt2" style="display: none;">&nbsp;</div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- Editable table -->
          </div>
        </section>
      </div>
      <?php 
        include'layout/modal_user.php';
        include'layout/modal_add_user.php';
      ?>
    </div>
    <!--libs vuejs -->
   	<script src="../public/js/vueTableUsuario.js"></script>
  </body>
</html>