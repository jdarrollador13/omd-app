<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>OMD</title>
    <link rel="stylesheet" type="text/css" href="../public/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
      html,body{
        background-image: url('../public/images/home3.png');
        /*background-size: cover;*/
        background-size: 100% 100%;
        background-repeat: no-repeat;
        height: 100%;
        /*font-family: 'Numans', sans-serif;*/
      }
    </style>
  <body>
  <!--Navbar -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <a class="navbar-brand" href="#">Inicio</a>

	  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
	    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
	      <li class="nav-item active">
	        <a class="nav-link" href="#">Crear Tarea <span class="sr-only"></span></a>
	      </li>
	      <li class="nav-item active">
	        <a class="nav-link" href="#">Listar Tareas<span class="sr-only"></span></a>
	      </li>
	      <li class="nav-item active">
	        <a class="nav-link" href="list_user.php">Listar Usuarios<span class="sr-only"></span></a>
	      </li>
	    </ul>
	    <form class="form-inline my-2 my-lg-0">
	      <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Search">
	      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
	    </form>
	  </div>
	</nav>
    <?php

    ?>
<!--/.Navbar -->
  <script src="public/vue/vue.js"></script>
  <script src="public/js/authentic.js"></script>
  </body>
</html>