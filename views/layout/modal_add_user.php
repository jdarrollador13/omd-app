<!-- The Modal -->
<div class="modal" id="myModalUser" v-if="modalAddUser">
  <div class="modal-dialog modal-dialog-custom">
    <div class="modal-content modal-content-cumston">
    
      <div class="modal-header">
        <h4 class="modal-title">Crear Usuario</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      
      <div class="modal-body">
        <blockquote class="blockquote mb-0">
          <form @submit.prevent="addUserForm">

            <fieldset>

              <div class="form-group row row-form">
                <div class="col-md-6" >
                	<label>Nombres:</label>
                  <input v-model="dataAddUser.nom_usuario" type="text" placeholder="Nombres" name="nombres"
                    v-validate="'required'" class="form-control">
                  <span class="error" v-if="errors.has('nombres')">{{errors.first('nombres')}}</span>
                </div>

                <div class="col-md-6">
                	<label>Apellidos:</label>
                  <input name="Apellidos" v-model="dataAddUser.ape_usuario" type="text" placeholder="Apellidos" 
                    v-validate="'required'" class="form-control">
                  <span class="error" v-if="errors.has('Apellidos')">{{errors.first('Apellidos')}}</span>
                </div>
              </div>

              <div class="form-group row row-form">
                <div class="col-md-6" >
                	<label>Usuario:</label>
                  <input v-model="dataAddUser.usuario" type="text" placeholder="Usuario" name="Usuario"
                    v-validate="'required'" class="form-control">
                  <span class="error" v-if="errors.has('Usuario')">{{errors.first('Usuario')}}</span>
                </div>

                <div class="col-md-6">
                	<label>Contraseña:</label>
                  <input name="Contraseña" v-model="dataAddUser.password" type="password" placeholder="Contraseña" 
                    v-validate="'required'" class="form-control">
                  <span class="error" v-if="errors.has('Contraseña')">{{errors.first('Contraseña')}}</span>
                </div>
              </div>

              <div class="form-group row row-form">
                <div class="col-md-12" >
                	<label>Cedula:</label>
                  <input v-model="dataAddUser.num_documento" type="text" placeholder="Cedula" name="Cedula"
                    v-validate="'required'" class="form-control">
                  <span class="error" v-if="errors.has('Cedula')">{{errors.first('Cedula')}}</span>
                </div>
              </div>
              
              <div class="form-group row row-form">
                <div class="col-md-6">
                	<label>Perfil:</label>
                  <select class="browser-default custom-select form-control" name="Perfil" v-model="dataAddUser.id_perfil" v-validate="'required'">
                    <option selected v-for="(option, index) in dataPerfil" v-bind:value="option.id">{{ option.nom_perfil }}</option>
                  </select>
                  <span class="error" v-if="errors.has('Perfil')">{{errors.first('Perfil')}}</span>

                </div>

                <div class="col-md-6">
									<label>Area:</label>
                  <select class="browser-default custom-select form-control" v-model="dataAddUser.id_area" name="Area" v-validate="'required'">
                    <option selected v-for="(option, index) in dataArea" v-bind:value="option.id">{{ option.nom_area }}</option>
                  </select>
                  <span class="error" v-if="errors.has('Area')">{{errors.first('Area')}}</span>
                </div>
              </div>

              <!--<div class="form-group row row-form">
                <div class="col-md-12">
                  <select class="browser-default custom-select form-control" name="Activo"v-model="dataEditUser.activ" v-validate="'required'">
                    <option selected v-for="(option, index) in dataActivo" v-bind:value="option.id">{{ option.activo }}</option>
                  </select>
                  <span class="error" v-if="errors.has('Activo')">{{errors.first('Activo')}}</span>
                </div>
              </div> -->

              <div class="form-group row">
                <div class="col-md-12 text-center">
                  <button type="submit" class="btn btn-primary btn-lg btn-form btn-add-user">Guardar</button>
                </div>
              </div>
            </fieldset>
            
            
          </form>
        </blockquote>
      </div>
      
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div> -->
      
    </div>
  </div>
</div>