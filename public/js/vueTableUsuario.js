//valida los formularios si los campos son requeridos
Vue.use(VeeValidate);
//instancia de vuejs con el id de la pagina  de usuarios
var app3 = new Vue({
  el: '#app-table',
  data : {
  		message : '',
	  	error : false,
	  	tableBody : false,
	  	tableData : [],
	  	dataPerfil : [],
	  	dataArea : [],
	  	dataActivo : [],

	  	modalUser : false,
	  	dataEditUser : {
	  		id : null,
	  		activo : true,
	  		nom_area : null,
	  		nom_perfil : null,
	  		nom_usuario : null,
	  		ape_usuario : null, 
	  	},

	  	modalAddUser : false,
	  	dataAddUser : {
	  		nom_usuario : null,
	  		ape_usuario : null,
	  		usuario : null,
	  		password : null,
	  		num_documento : null,
	  		id_area : null,
	  		id_perfil : null,
	  	}
  },

  mounted : function (){
  	this.getData();
  	this.getAllData();
  },

  methods : {

  	getData: function () {
  		fetch('../../../app_omd/controllers/router.php?accion=getUsers', {
			  method: 'GET',
			  headers:{
			    'Content-Type': 'application/json'
			  }
			})
			.then(res => res.json())
			.catch(error => console.error('Error:', error))
			.then(response => {
				let status = response.status;
				if(status == 200){
					this.tableBody = true;
					this.tableData = response.data;
				}else if(status == 201){

				}else if(status == 500){
					
				}
			});
  	},

  	editUser: function (dataUser) {
  		this.dataEditUser = Object.assign({}, dataUser);
  		//this.getAllData();
  		this.modalAddUser = false;
  		this.modalUser = true;
  	},

  	editUserForm: function(){
  		this.$validator.validateAll().then(() => {
  			fetch('../../../app_omd/controllers/router.php?accion=editUser', {
				  method: 'POST', 
				  body: JSON.stringify(this.dataEditUser), // data can be `string` or {object}!
				  headers:{
				    'Content-Type': 'application/json'
				  }
				})
				.then(res => res.json())
				.catch(error => console.error('Error:', error))
				.then(async  response => {
					let resSwall = "";
					let status = response.status;
					if(status == 200){
						resSwall = await this.swallInfo('Se actualizo correctamente la información...', 'success');
						window.location.href = 'list_user.php';
					}else if(status == 201){
						resSwall = await this.swallInfo('No hubo modificación de datos...', 'info');
						window.location.href = 'list_user.php';
					}else if(status == 500){
						resSwall = await this.swallInfo('No se Gurdo la información...', 'info');
					}
				});

	      }).catch(() => {
	        console.log(this.errors.all())
	        return false
	      })
  		//if (!this.errors.any()) {}
  	},

  	addUser: function() {
			this.dataAddUser.id_perfil = this.dataPerfil[0].id;
  		this.dataAddUser.id_area = this.dataArea[0].id;
  		this.modalUser = false;
  		this.modalAddUser = true;
  	},

  	addUserForm: function() {
  		this.$validator.validateAll().then(() => {
  			fetch('../../../app_omd/controllers/router.php?accion=addUser', {
				  method: 'POST', 
				  body: JSON.stringify(this.dataAddUser), // data can be `string` or {object}!
				  headers:{
				    'Content-Type': 'application/json'
				  }
				})
				.then(res => res.json())
				.catch(error => console.error('Error:', error))
				.then(async  response => {
					let resSwall = "";
					let status = response.status;
					if(status == 200){
						console.log('pppppp')
					}else if(status == 201){

					}else if(status == 500){

					}
				});

	      }).catch(() => {
	        console.log(this.errors.all())
	        return false
	      })
  	},

  	getAllData: function() {
  		fetch('../../../app_omd/controllers/router.php?accion=getAllData', {
			  method: 'GET',
			  headers:{
			    'Content-Type': 'application/json'
			  }
			})
			.then(res => res.json())
			.catch(error => console.error('Error:', error))
			.then(response => {
				let status = response.status;
				if(status == 200){
					this.dataActivo = [
						{ activo : 'Activo', id : 1 },
						{ activo : 'Inactivo', id : 0 }
					];
					this.dataPerfil = response.perfil;
					this.dataArea = response.areas;
				}else if(status == 201){

				}else if(status == 500){
					
				}
			});

  	},

  	swallInfo: async function(title, icon){
  	 await	swal({
						  title: title,
						  icon: icon,
						  button: "Aceptar",
						});
  	 return true;
  	}
  },
   
  created () {
  	
  }
})