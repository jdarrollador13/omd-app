<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>OMD</title>
  <link rel="stylesheet" type="text/css" href="public/bootstrap/css/bootstrap.min.css">
	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="public/css/style.css">
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
  <body>
    <div class="container" id="app-loguin">
      <div class="d-flex justify-content-center h-100">
        
        <div class="card">
          
        <div class="alert alert-danger error-loguin" id="error-loguin" role="alert" v-if="error">{{ message }}</div>
          <div class="card-header">
              <h3>Ingresar</h3>
          </div>
          <div class="card-body" style="margin-top: 13px;">
            <form action="post" v-on:submit.prevent="validateUsers">
              <div class="input-group form-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-user"></i></span>
                </div>
                <input type="text" v-model="dataUser.userName" name="name" id="name" class="form-control" placeholder="usuario">

              </div>
              <div class="input-group form-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-key"></i></span>
                </div>
                <input class="form-control" v-model="dataUser.userPass" type="password" name="password" id="pasword" placeholder="clave">
              </div>
              <div class="form-group">
                <input class="btn float-right login_btn" type="submit" name="accion" id="loguin" value="Ingresar">
              </div>
            </form>
          </div>
          <div class="card-footer">
              
          </div>
        </div>
      </div>
    </div>
    <!--<script src="public/js/authen.js"></script>-->
    <script src="public/vue/vue.js"></script>
    <script src="public/js/authentic.js"></script>
  </body>
</html>